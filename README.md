
##Ruby on Rails EEMI blog 

#Version 1.0.0

#Équipe

    Quentin Faure
    Laurent Maximin

#Fonctionalités

    Affichage des articles
    Possibilité d'ajouter un nouvel article avec une image et des catégories
    Possibilité de les éditer ou supprimer
    Possibilité de rechercher des articles en fonction des titres
    Possibilité de laisser des commentaires sur les articles
    Possibilité de modérer les commentaires
	Possibilité de créer des catégories
    Inscription et connexion
    Vérification de la connexion
    Notifications
    Affichage de la liste des utilisateurs
    Possibilité de les supprimer (admin)
	Gestion du bandeau de la CNIL avec cookie (gem cookies_eu)
	Dashboard admin (gem administrate)
	Utilisation de rails_12factor pour la production (heroku)
	

#Heroku

#https://pacific-depths-47939.herokuapp.com/
#Connexion en tant qu'Admin 
- Login : john@example.com 
- Password : password 

#https://pacific-depths-47939.herokuapp.com/admin pour l'accès au dashboard admin