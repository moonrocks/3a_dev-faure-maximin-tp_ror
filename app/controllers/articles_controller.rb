class ArticlesController < ApplicationController

  before_action :setArticle, only: [:edit, :update, :show, :destroy]
  before_action :require_user, except: [:index, :show]
  before_action :require_same_user, only: [:edit, :update, :destroy]

  # def index
  #   @all_articles =  Article.all
  # end

  def index
    @articles = if params[:term]
      Article.where('title LIKE ?', "%#{params[:term]}%")
    else
      @all_articles = Article.all
    end
  end

  def search
    # render plain: params[:q].inspect
    @articles = Article.search(Article.arel_table[:title].matches("%#{params[:q]}%"))
    render 'index'
  end

  def task_params
    params.require(:article).permit(:title, :descritpion)
  end
  def new
    @article = Article.new
  end

  def edit

  end

  def create
    #render plain: params[:article].inspect
    @article = Article.new(article_params)
    @article.user = current_user
    if @article.save
      flash[:success] = "Article was succesfully created!"
      redirect_to article_path(@article)
    else
      render :new
    end

  end

  def update
    if @article.update(article_params)
      flash[:success] = "Article was succesfully updated!"
      redirect_to article_path(@article)
    else
      render :edit
    end
  end

  def show
    @comment = Comment.new
  end

  def destroy
    @article.destroy
    redirect_to articles_path
    flash[:danger] = "Article succesfully deleted!"
  end

  private

    def article_params
       params.require(:article).permit(:title, :image, :description, category_ids: [])
    end

    def setArticle
      @article = Article.find(params[:id])
    end

    def require_same_user
      if current_user != @article.user and !current_user.admin?
        flash[:danger] = "You can only edit/delete your own articles!"
        redirect_to login_path
      end
    end



end