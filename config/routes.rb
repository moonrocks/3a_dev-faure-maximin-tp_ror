Rails.application.routes.draw do
  namespace :admin do
    resources :articles
resources :article_categories
resources :categories
resources :comments
resources :users

    root to: "articles#index"
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  resources :articles

  root 'pages#home'
  get 'about', to: 'pages#about'
  get 'signup', to: 'users#new'
  # Page de recherche
  get '/search', to: 'articles#search'

  resources :users, except: [:new]

  get 'login', to: 'sessions#new'
  post 'login', to: 'sessions#create'
  delete  'logout', to: 'sessions#destroy'

  resources :categories
  resources :comments, except: [:index, :show, :destroy, :edit, :update]

end
